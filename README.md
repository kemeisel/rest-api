REST API client
====================================

The REST API client is a JavaScript implementation designed to make an arbitrary number of calls to a paginated API endpoint and transform the resulting payload according to specified rules.

For additional information, see the [original specification][specification].


Getting Started
------------------------------------

### Prerequisites
You'll need to install [git][git] to clone this repository:
```
sudo apt install git

```

You'll also need Jasmin-node to run the tests.  See [npmjs][npmjs] for information on installing Node.js and updating npm.  Once installed, leverage npm to install [Jasmine-node][Jasmine-node]:
```
npm install jasmine-node -g
```

### Installing

Installation is as simple as cloning the repository...

```
git clone https://kemeisel@bitbucket.org/kemeisel/rest-api.git
cd rest-api
```

Running the tests
------------------------------------
From the root directory, run:
```
jasmine-node --color ./
```

Jasmine-node may be configured to watch both the source and test files for changes, running the tests whenever either is saved.  To enable this functionality:
```
jasmine-node --autotest --color ./
```


Next Steps
------------------------------------
- Most notably, the REST API doesn't function across domains.  Rather than digging into a Cross-Resource Resource Sharing (CORS) implementation, I thought it better to get the code into your hands as soon as possible.  I _greatly_ suspect a tool such as Nutshell has crossed this bridge already.
- The testing specification is incomplete, stopping just before the point of testing the API call itself.  Again, I thought it prudent to deliver early rather than complicate the code and installation by including JQuery or making a manual XMLHttpRequest.


[specification]: https://github.com/nutshellcrm/join-the-team/blob/master/developer-questions.md#rest-api-client
[git]: https://git-scm.com/book/en/v2/Getting-Started-Installing-Git
[npmjs]: https://docs.npmjs.com/getting-started/installing-node
[Jasmine-node]: https://github.com/mhevery/jasmine-node
