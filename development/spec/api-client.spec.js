// api-client.spec.js

var ApiClient = require('../lib/api-client');

describe('ApiClient()', function(){
  var apiClient = new ApiClient('http://localdev/people/');

  var end_point_data = [
    {name: "Dan Truman",
      email: null,
      signup_date: "2001-9-28"},
    {name: "Matt Jackson",
      email: "mattjackson@example.com",
      signup_date: "2009-4-24"},
    {name: "Dan Bush",
      email: "danbush@example.com",
      signup_date: "2010-5-28"},
    {name: "Rebekka Roosevelt",
      email: null,
      signup_date: "2011-12-8"},
    {name: "Guy Adams",
      email: "guyadams@example.com",
      signup_date: "2000-1-24"}
  ];

  it('sorts array by signup_date', function(){
    var target = [{
      name: "Rebekka Roosevelt",
      email: null,
      signup_date: "2011-12-8"
    }];

    expect(end_point_data.sort(apiClient.order_by_signup_desc).slice(0,1)).toEqual(target);
  });

  it('limits bucket entries to those with a valid email', function(){
    apiClient.clearBucket();
    apiClient.fillBucket(end_point_data);
    expect(apiClient.bucket.length).toEqual(3);
  });

  it('clears bucket', function(){
    apiClient.fillBucket(end_point_data);
    expect(apiClient.bucket.length).toBeGreaterThan(0);

    apiClient.clearBucket();
    expect(apiClient.bucket.length).toEqual(0);
  })

  xit('retrieves data from the endpoint', function(){
    apiClient.clearBucket();
    apiClient.requestPage(1);
    expect(apiClient.bucket.length).toBeGreaterThan(0);
  });
});
