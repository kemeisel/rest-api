// api-client.js
var ApiClient = function(targetURL){
  this.targetURL = targetURL;
  this.bucket = [];
};

ApiClient.prototype.order_by_signup_desc = function(a,b) {
  a = new Date(a.signup_date);
  b = new Date(b.signup_date);
  return a > b ? -1 : a < b ? 1 : 0;
};

ApiClient.prototype.fillBucket = function(data_payload) {
  var self = this;
  for(i=0; i<data_payload.length; i++){
    if(!!data_payload[i].email){
      self.bucket.push(data_payload[i]);
    }
  }
};

ApiClient.prototype.clearBucket = function() {
  var self = this;
  self.bucket = [];
};

ApiClient.prototype.requestPage = function(page_number) {
  var self = this;
  var promise = $.ajax({
    url: self.targetURL + page_number,
    async: false
  });
  promise.done(function(data){
    if(data.length > 0){
      self.fillBucket(data);
      self.requestPage(page_number + 1);
    }
  });
};

ApiClient.prototype.displayBucket = function(payload) {
  var self = this;
  var top_five = payload.sort(self.order_by_signup_desc).slice(0,5);
  var html = "";

  html += "<table>";
  html += "<thead><tr><th>Name</th><th>Signup date</th><th>Email</th></thead>";
  html += "<tbody id='signups'>"

  $.each(top_five, function(index, value){
    signup_display = new Date(value.signup_date)
    html += "<tr>";
    html += "<td>" + value.name + "</td>";
    html += "<td>" + signup_display.toISOString().substring(0, 10) + "</td>";
    html += "<td>" + value.email + "</td>";
    html += "</tr>";
  });

  html += "</tbody>";
  html += "</table>";

  return html;
};

module.exports = ApiClient;
